package com.qburst;

/**
 * Created by nithin on 23/4/15.
 */

import com.qburst.core.CassandraData;
import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import io.dropwizard.db.DataSourceFactory;

import java.util.Collections;
import java.util.Map;

public class ExampleConfiguration extends Configuration {
    @NotNull
    private Map<String, Map<String, String>> viewRendererConfiguration = Collections.emptyMap();

    @Valid
    @NotNull
    @JsonProperty("CassandraData")
    private CassandraData cassandraData;

    public CassandraData getCassandraData() {
        return cassandraData;
    }

    @JsonProperty("viewRendererConfiguration")
    public Map<String, Map<String, String>> getViewRendererConfiguration() {
        return viewRendererConfiguration;
    }
}
