package com.qburst.views;

import io.dropwizard.views.View;

/**
 * Created by nithin on 23/4/15.
 */
public class signUpView extends View {
//    private final UserData user;
    private final String message;
    private final String name;

    public signUpView() {
        super("signUp.ftl");
        this.message=" ";
        this.name=" ";
    }

    public signUpView(String name,String message) {
        super("signUp.ftl");
        this.name=name;
        this.message=message;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }
}
