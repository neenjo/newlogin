package com.qburst.views;

import io.dropwizard.views.View;

/**
 * Created by nithin on 27/4/15.
 */
public class LoginView extends View {
    String message;
    public LoginView()
    {
        super("login.ftl");
        this.message="";

    }

    public LoginView(String message)
    {
        super("login.ftl");
        this.message=message;

    }

    public String getMessage() {
        return message;
    }
}
