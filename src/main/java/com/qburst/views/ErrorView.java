package com.qburst.views;

/**
 * Created by nithin on 4/5/15.
 */

import io.dropwizard.views.View;

public class ErrorView extends View {
    private String msg;

    public ErrorView(RuntimeException msg) {
        super("error.ftl");
        this.msg = msg.getMessage();
    }

    public String getMsg() {
        return msg;
    }
}