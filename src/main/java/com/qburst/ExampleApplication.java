package com.qburst;

import com.qburst.dao.CassandraConnectionConfiguration;
import com.qburst.resource.ExampleResource;
import com.qburst.dao.MyDao;
import com.qburst.utils.RunTimeExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.Application;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;
import io.dropwizard.assets.AssetsBundle;
import com.datastax.driver.mapping.MappingManager;

import java.util.Map;


/**
 * Created by nithin on 23/4/15.
 */
public class ExampleApplication extends Application<ExampleConfiguration> {
    public static void main(String[] args) throws Exception {
        new ExampleApplication().run(args);
    }

    public void initialize(Bootstrap<ExampleConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<ExampleConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(ExampleConfiguration config) {
                return config.getViewRendererConfiguration();
            }
        });
        bootstrap.addBundle(new AssetsBundle("/assets", "/public", null));
    }

    @Override
    public void run(ExampleConfiguration config, Environment environment) {
        MappingManager manager = new MappingManager(new CassandraConnectionConfiguration(config.getCassandraData().getHost(), config.getCassandraData().getKeyspace()).getSession());
        MyDao dao = manager.createAccessor(MyDao.class);

        environment.servlets().setSessionHandler(new SessionHandler());
        environment.jersey().register(new ExampleResource(dao));
        environment.jersey().register(new RunTimeExceptionMapper());
    }


}

