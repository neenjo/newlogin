package com.qburst.utils;

/**
 * Created by nithin on 4/5/15.
 */


        import com.qburst.views.ErrorView;

        import javax.ws.rs.core.Response;
        import javax.ws.rs.ext.ExceptionMapper;
        import javax.ws.rs.ext.Provider;

@Provider
public class RunTimeExceptionMapper implements ExceptionMapper<RuntimeException> {
    public Response toResponse(RuntimeException exception) {
        exception.printStackTrace();
        Response response = Response
                .serverError()
                .entity(new ErrorView(exception))
                .build();
        return response;
    }
}