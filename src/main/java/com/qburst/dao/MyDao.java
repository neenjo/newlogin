package com.qburst.dao;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.annotations.*;
import com.qburst.core.UserData;
import com.datastax.driver.mapping.annotations.Accessor;


import java.util.List;

/**
 * Created by nithin on 23/4/15.
 */
@Accessor
public interface MyDao {

    @Query("select * from users where username = :username and password = :password")
    UserData loginValidate(@Param("username") String username,@Param("password") String password);

    @Query("select * from users where username = :username")
    UserData findDuplicate(@Param("username") String username);

    @Query("insert into users (name,username,password)values (:name, :username, :password)")
    ResultSet addUser(@Param("name")String name,@Param("username") String username,@Param("password") String password);

}
