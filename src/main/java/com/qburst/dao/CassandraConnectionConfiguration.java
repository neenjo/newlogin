package com.qburst.dao;

/**
 * Created by nithin on 5/5/15.
 */
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class CassandraConnectionConfiguration {
    private Cluster cluster;
    private Session session;
    public Cluster getCluster()
    {
        return cluster;
    }
    public Session getSession()
    {
        return session;
    }
    public CassandraConnectionConfiguration(String host,String keyspace)
    {

        cluster = Cluster.builder().addContactPoint(host).build();
        session = cluster.connect(keyspace);
    }
}
