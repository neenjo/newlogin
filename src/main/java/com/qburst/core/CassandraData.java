package com.qburst.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by nithin on 5/5/15.
 */

public class CassandraData {
    @JsonProperty
    private String keyspace;
    @JsonProperty
    private String host;

    public String getKeyspace() {
        return keyspace;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
