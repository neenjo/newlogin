package com.qburst.resource;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.qburst.core.UserData;
import com.qburst.dao.MyDao;
import com.qburst.utils.HashUtils;
import com.qburst.utils.RoutingUtils;
import com.qburst.views.JoinedView;
import com.qburst.views.LoginView;
import com.qburst.views.signUpView;
import io.dropwizard.jersey.sessions.Session;
import io.dropwizard.views.View;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by nithin on 23/4/15.
 */
@Path("/")
public class ExampleResource {
    private MyDao dao;
    private String name = " ";
    private String message = "Welcome you are now logged in";
//    private ResultSet resultSet;

    public ExampleResource(MyDao dao) {
        this.dao = dao;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Object getUser(@Session HttpSession session) {
        if(RoutingUtils.authenticate(session)) {
            return new JoinedView(((String) session.getAttribute("username")), " Welcome");
        }
        else {
            return RoutingUtils.redirectToURI("/login");
        }
    }

    @Path("/logout")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String logouter(@Session HttpSession session) {
        session.setAttribute("username", "");
        System.out.println("inisdde post logout");
        session.invalidate();
 //       RoutingUtils.redirectToURI("/login");
        return "true";
    }


    @Path("/login")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public LoginView login() {
        return new LoginView();
    }

    @Path("/login")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String newFetch(@FormParam("username") String username, @FormParam("password") String password, @Session HttpSession session) {
        String hash = HashUtils.generateHash(password);
//        resultSet = dao.loginValidate(username, hash);
//        System.out.println("Enteresd username is "+username);
//     ;
//
//        List<Row> listDuplicate=resultSet.all();
//        System.out.println(listDuplicate);

        if (dao.loginValidate(username,hash)!=null) {
            System.out.println("Logged in");
            session.setAttribute("username", username);
//            RoutingUtils.redirectToURI("/");
            return "true";
        }
         return "The username or password you entered is incorrect";


    }


    @Path("/joined")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public JoinedView join() {
        return new JoinedView(name,message);
    }

    @Path("/joined")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public JoinedView logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.invalidate();
        RoutingUtils.redirectToURI("/login");
        return new JoinedView("");
    }

    @Path("/signup")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public signUpView getsignUpView() {
        return new signUpView();
    }

    @Path("/signup")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String fetch(@FormParam("name") String name, @FormParam("username") String username, @FormParam("password") String password, @FormParam("confirmPassword") String confirmPassword) {
//        resultSet= dao.findDuplicate(username);
//        List<Row> listDuplicate=resultSet.all();
        Pattern p = Pattern.compile("[^a-z0-9_ ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(username);
        boolean b = m.find();

        if (!password.equals(confirmPassword)) {
            return " Your password and confirmation password do not match. [******] - has been ignored.";
        } else if (dao.findDuplicate(username)!=null) {
            return "Username already in Use";
        } else if (username.trim().length() == 0 || password.trim().length() == 0 || name.trim().length() == 0) {
            return "Can't Accept Empty Fields";
        } else if (username.length() >= 40 || password.length() >= 40 || name.length() >= 40) {
            return "Field is too long -data ignored";
        } else if (b || username.contains(" ")) {
            return "Please use only letters (a-z), numbers, and periods.";

        } else if (password.length() <= 6) {
            return "Password: Too short ";

        } else {
            System.out.println(password);
            System.out.println(HashUtils.generateHash(password));
            String hash = HashUtils.generateHash(password);
            System.out.println(hash);
            dao.addUser(name,username,hash);

         //   RoutingUtils.redirectToURI("/joined");
            return "true";
        }

    }


}
