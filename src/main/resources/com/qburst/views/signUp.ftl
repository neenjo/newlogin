<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign Up Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">



<div class="form-horizontal" id="sign up">
 <h1>Sign up</h1>

<div class="form-group">
     <div class="col-sm-offset-0 col-sm-4">
       <input type="text" class="form-control" id="name" placeholder="name" name="name">
     </div>
   </div>
   <div class="form-group">
    <div class="col-sm-offset-0 col-sm-4">
          <input type="text" class="form-control" id="username" placeholder="Username" name="username">
        </div>
      </div>
      <div class="form-group">

        <div class="col-sm-offset-0 col-sm-4">
          <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        </div>
      </div>
 <div class="form-group">
     <div class="col-sm-offset-0 col-sm-4">
       <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" name="confirmPassword">
     </div>
   </div>

   <div class="form-group">
     <div class="col-sm-offset-0 col-sm-4">
       <button class="btn btn-default" id="signup">Sign Up</button>
     </div>
   </div>
<div class="col-sm-12 form-group">
     <h6 id="message" class="col-sm-4 alert alert-danger ">${message}</h6>
   <div class="form-group">

    </div>
       <div class="col-sm-12"></div>
 <h5>Already have an Account? <a href="login">Login</a></h5>
 </div>

 </div>

	<script src="public/js/jquery-2.1.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="public/js/signup.js"></script>
 </body>
 </html>