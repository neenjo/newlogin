<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign Up Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">

<div class="form-horizontal" id="sign in">
 <h1>Login </h1>
   <div class="form-group">

     <div class="col-sm-4">
       <input type="text" class="form-control" id="username" placeholder="Username" name="username">
     </div>
   </div>
   <div class="form-group">

     <div class="col-sm-4">
       <input type="password" class="form-control" id="password" placeholder="Password" name="password">
     </div>
   </div>

   <div class="form-group">
     <div class="col-sm-offset-0 col-sm-4">
       <button type="submit" class="btn btn-default" id="login">Sign in</button>
     </div>
   </div>
    <div class="col-sm-12 form-group">
    <h6 id="message" class="col-sm-4 alert alert-info">${message}</h6>
    </div>

     <h6><a href="/signup"> Create An Account ???</a></h6>
 </div>

 </div>

	<script src="public/js/jquery-2.1.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="public/js/login.js"></script>
 </body>
 </html>