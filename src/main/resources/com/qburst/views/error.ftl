<!DOCTYPE html>
<html lang="en">
<head>
    <title>Error</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
</head>
<body>

	<h4><span class="label label-danger col-sm-offset-2" role="alert">${msg}</span></h4>
		<h2 class="col-sm-offset-2">Oops Something Went Wrong !!</h2>
		<h5 class="col-sm-offset-2"><a href="/signup">Signup</a></h5>
		<h5 class="col-sm-offset-2"><a href="/login">Login</a></h5>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>