$(document).ready(function () {
	$('#login').click(function () {
		if ($('#username').val().trim() !== "" && $('#password').val().trim() !== "") {
			$.post( "/login", {
				username: $('#username').val(),
				password: $('#password').val()
			}).done(function(data) {
				if (data === "true") {
					window.location.href = '/';
				} else {
					$('#message').text(data);
				}
			});
		} else {
			$('#message').text("Please fill all the fields.");
		}
	});
});