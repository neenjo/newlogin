$(document).ready(function () {
	$('#signup').click(function () {
		if ($('#username').val().trim() !== "" && $('#password').val().trim() !== "" && $('#confirmPassword').val().trim() !== "") {
			$.post( "/signup", {
				name:$('#name').val(),
				username: $('#username').val(),
				password: $('#password').val(),
				confirmPassword: $('#confirmPassword').val()
			}).done(function(data) {
				if (data === "true") {
					window.location.href = '/joined';
				}
				else {
					$('#message').text(data);
				}
			});
		} else {
			$('#message').text("Please fill all the fields.");
		}
	});
});